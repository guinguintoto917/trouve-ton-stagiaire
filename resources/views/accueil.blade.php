@extends('template')

@section('title', 'Accueil')

@section('content')
<style>
input::-webkit-calendar-picker-indicator {
  position: absolute;
  width: 100%;
  height: 100%;
  opacity: 0;
}

@media (max-width: 768px) {
    .w-75 {
        width: 30%!important;
      }
    .captcha {
      padding: 1rem;
    }
}
</style>

  <div id="titre" class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center text-white bg-light" style="background-color: #49894B!important;box-shadow: 4px 4px 6px gray;">
      <div class="row">
            <div class="col-md-6 p-lg-5">
                <img style="width: 80%;" src="{{ asset('storage/logo-sans-fond.png') }}"></img>
                <h2 class="text-center">Le choix à portée de main</h2>
            </div>
            <div class="col-md-6">
                <img class="img_logo" style="width: 40%;" src="{{ asset('storage/logo.png') }}"></img>
            </div>
      </div>
  </div>

  <div id="domaine" class="position-relative overflow-hidden p-3 p-md-5 m-md-3 bg-light" style="background-color: #084F7A!important;box-shadow: 4px 4px 6px gray;">
    <h2 class="text-center p-3 text-white">Nos différents domaines de formations</h2>
    <div class="container-fluid">
    </div>
    <div class="card-deck">
        @foreach($domaines as $domaine)
      <div class="card card-custom bg-white border-white">
          <div class="card-custom-img" style="background-image: url({{ asset('/') }}storage/{{ $domaine->image }});"></div>
          <div class="card-custom-avatar">
            <img class="img-fluid" src="{{ asset('/') }}storage/{{ $domaine->logo }}" alt="Icone" />
          </div>
          <div class="card-body" style="overflow-y: auto">
          <h4 class="card-title" style="text-align: center">{{$domaine->nom}}</h4>
            <p class="card-text text-center">{{$domaine->description}}</p>
          </div>
          <div class="card-footer text-center" style="background: inherit; border-color: inherit;">
            <a class="btn btn-outline-dark" href="{{ route('domaine.show', ['domaine' => $domaine->id]) }}" role="button">Ici toutes les actus stages</a>
          </div>
        </div>
        @endforeach
    </div>
    <form action="{{ route('search') }}" method="POST">
    @csrf
      <div class="row">
      <div class="col-md-12 pt-3">
        <h3 class="text-center p-3 text-white">Faites nous part de votre recherche par date ou par mots clés</h3>
      </div>
      <div class="col-md-6">
        <div class="input-group mb-3">
          <input class="form-control form-control-lg" type="date" name="rechercheDate" max="3000-12-31"min="1000-01-01" class="form-control">
          <div class="input-group-append">
            <span class="input-group-text" id="datepickericon"><i class="far fa-calendar-alt"></i></span>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="input-group mb-3">
            <input type="text" class="form-control form-control-lg" name="recherche" placeholder="Mots clés : Commerce, Info, BTS SIO">
            <div class="input-group-append">
              <button class="btn btn-success" type="submit"><i class="fas fa-search"></i></button>
            </div>
        </div>
      </div>
    </div>
    </form>
    <h2 class="text-center p-3 text-white">Les stages à venir</h2>
    <div class="card-deck">
    @foreach($stages as $s)
    <div class="card">
        <div class="card-body">
        <h6 class="card-title text-center">{{ $s->promotion->domaine->nom }}</h6>
          <p class="card-text text-center"><span class="badge badge-success">{{ Carbon\Carbon::parse($s->dateDebut)->format('d-m-Y') }}</span> au <span class="badge badge-success">{{ Carbon\Carbon::parse($s->dateFin)->format('d-m-Y') }}</p>
          <p class="card-text text-center">{{ $s->promotion->nom }}</p>
          <p class="card-text text-center">{{ $s->promotion->description }}</p>
          <div class="text-center">
          <form method="POST" action="{{ route('search') }}">
            @csrf
            <input name="statut" type="hidden" value="show">
            <input name="recherche" type="hidden" value="{{ $s->promotion->nom }}">
            <button type="submit" class="btn btn-success"><i class="far fa-eye"></i> Ouvrir les détails</button>
          </form>
          </div>
        </div>
      </div>
    @endforeach
    </div>
  </div>
</div>

  <div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
    <div id="carte" class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden" style="
    background-color:#921853!important;box-shadow: 4px 4px 6px gray;">
      <div class="my-3 py-3">
        <img src="{{url ('../public/storage/logo-lycee.png')}}" style="width: 100%;"></img>
      </div>
    </div>
    <div id="carte"class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden" style="
    background-color:#C79129!important;box-shadow: 4px 4px 6px gray;">
      <div class="my-3 p-3">
        <h2 class="display-5">Le lycée</h2>
        <p class="lead">Situé au centre-ville de Dole dans le Jura, le Lycée Pasteur Mont Roland offre un panel de formations
                         diverses et variées allant de la 3e jusqu'à la Licence en passant par le Bac et le BTS. Il accueille
                         1100 élèves dont 400 en formation post-bac de Bac+2 à Bac+4. Les formations qu'il propose sont regroupées
                         en 4 grands domaines qui sont le numérique, la santé & le social, l'art & le design et le commerce & le management</p>
      </div>
    </div>
  </div>
    <div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3" id="contact">
        <div id="titre" class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden" style="background-color: #82B137!important;box-shadow: 4px 4px 6px gray;">
            <div class="my-3 py-3">
            <h2 class="display-5">Où nous-trouver</h2>
            <div class="map-responsive">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2716.238034387861!2d5.491666015489311!3d47.09440407915407!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478d4c97319a022b%3A0x10455424f077465e!2sLycee%20Pasteur%20Mont%20Roland%20P%C3%B4le%20Alternance%20Formation!5e0!3m2!1sfr!2sfr!4v1583831803907!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
            </div>
        </div>
        <div id="carte"class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden" style="box-shadow: 4px 4px 6px gray;">
          <div class="my-3 p-3">
              <h2 class="display-5">Nous contacter</h2>
          </br>
          <p class="lead text-center">Une remarque, une suggestion, un renseignement ? Remplissez ce formulaire pour toutes demandes annexes au site</p>
          @if ($errors->any())
          <div class="col-md-12 text-center p-3">
            <div class="alert alert-danger" role="alert">
              @foreach ($errors->all() as $error)
                  <p style="margin-bottom: 0rem;"><i class="fas fa-times"></i> {{ $error }}</p>
              @endforeach
            </div>
          </div>
          @endif
          @if (session('status'))
          <div class="col-md-12 text-center p-3">
            <div class="alert alert-success" role="alert">
                <i class="fas fa-check"></i> {{ session('status') }}
            </div>
          </div>
          @endif
              <form action="{{ route('renseignement') }}" method="POST">
                  @csrf
                  <div class="form-row p-2">
                      <div class="col-md-3">
                          <label>Identité</label>
                      </div>
                      <div class="col-md-9">
                          <input type="text" placeholder="Exemple : Marie Dupont" class="form-control" name="identite" value="{{ old('identite') }}" required>
                      </div>
                  </div>
                  <div class="form-row p-2">
                      <div class="col-md-3">
                          <label>Adresse mail</label>
                      </div>
                      <div class="col-md-9">
                          <input type="email" placeholder="Exemple : contact@1mix.com" class="form-control" name="email" value="{{ old('email') }}" required>
                      </div>
                  </div>
                  <div class="form-row p-2">
                      <div class="col-md-3">
                          <label>Votre message</label>
                      </div>
                      <div class="col-md-9">
                          <textarea class="form-control" maxlength="1500" name="message" rows="5" required>{{ old('message') }}</textarea>
                      </div>
                  </div>
                  <div class="form-row p-2">
                      <div class="col-md-3">
                          <label>Captcha</label>
                      </div>
                      <div class="col-md-6">
                        <input type="text" class="form-control" name="captcha" minlength="6" maxlength="6" required>
                      </div>
                      <div class="col-md-3 captcha">
                          <img class="w-75 img-thumbnail" src="{!! Captcha::src() !!}"></img>
                      </div>
                  </div>
              <button class="btn btn-success" type="submit"><i class="fas fa-paper-plane"></i> Envoyer</a>
              </form>
          </div>
        </div>
    </div>
  <footer class="container py-5">
    <div class="row">
      <div class="col-12 col-md">
        <small class="d-block mb-3 text-muted">Trouve ton stagiaire &copy; 2020 - Réalisé avec <i class="fas fa-heart"></i> par Mathis Djekhar, Thomas Guinchard, Remy Fejoz, Calvin Massot</small>
      </div>
    </div>
  </footer>
@stop
