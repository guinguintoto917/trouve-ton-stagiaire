@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Administration</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bienvenue <b>{{ Auth::user()->name }}</b>, vous êtes connecté !
                </div>
            </div>
        </div>

        <div class="col-md-12" style="padding: 10px;">
          <div class="card-deck">
            <div class="card">
              <img class="card-img-top" src="{{url('/storage/stagiaire.jpg')}}" alt="Gestion des stages">
              <div class="card-body text-center">
                <h5 class="card-title">Gestion des stages</h5>
                <a class="btn btn-success w-100" href="{{ route('stages') }}" role="button"><i class="fas fa-cogs"></i> Gérer les stages</a>
              </div>
            </div>
            <div class="card">
              <img class="card-img-top" src="{{url('/storage/domaines.jpg')}}" alt="Gestion des domaines">
              <div class="card-body text-center">
                <h5 class="card-title">Gestion des domaines</h5>
                <a class="btn btn-success w-100" href="{{ route('domaines') }}" role="button"><i class="fas fa-cogs"></i> Gérer les domaines</a>
              </div>
            </div>
            <div class="card">
              <img class="card-img-top" src="{{url('/storage/promotions.jpg')}}" alt="Gestion des promotions">
              <div class="card-body text-center">
                <h5 class="card-title">Gestion des promotions</h5>
                <a class="btn btn-success w-100" href="{{ route('promotions') }}" role="button"><i class="fas fa-cogs"></i> Gérer les promotions</a>
              </div>
            </div>
          </div>
        </div>
    </div>

@endsection
