@extends('template')

@section('title', $domaines->nom)

@section('content')
<link rel="stylesheet" href="{{ asset('/css/landing-page.css') }}">

<style>
header.masthead {
    position: relative;
    background-color: #343a40;
    background: url(../storage/{{ $domaines->image }}) no-repeat center center;
    background-size: cover;
    padding-top: 8rem;
    padding-bottom: 8rem;
}

.btn-link, .btn-link:hover, .btn-link:active, .btn-link:visited, .btn-link:focus {
    font-weight: 400;
    color: #000000;
    text-decoration: none;
}
</style>

@foreach($domaines->promotion as $p)
<div class="modal fade" id="i{{ $p->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="i{{ $p->id }}">Information du {{ $p->nom }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        {{ $p->description }}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Fermer</button>
      </div>
    </div>
  </div>
</div>
@endforeach

<header class="masthead text-white text-center">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-xl-9 mx-auto">
        <h1 class="mb-5">{{ $domaines->nom }}</h1>
      </div>
    </div>
  </div>
</header>

<section class="testimonials text-center bg-light">
    <div class="container">
      <h3 class="mb-5">Les stages du {{ strtolower($domaines->nom) }}</h3>
      <div class="d-flex justify-content-center">
        <a class="btn btn-danger" href="{{ route('accueil') }}" role="button"><i class="fas fa-arrow-circle-left"></i> Retour</a>
      </div>
      <div class="row">
        <div class="col-md-12 pt-3">
          <div class="accordion" id="domaines">
            @forelse($domaines->promotion as $p)
              @forelse($p->stage as $s)
                @php ($i = 1)
            <div class="card">
              <div class="card-header">
                <h2 class="mb-0">
                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#o{{ $s->id }}">
                   <span class="badge badge-danger"><i class="fas fa-graduation-cap"></i> {{ $p->nom }}</span> - Du <span class="badge badge-success">{{ Carbon\Carbon::parse($s->dateDebut)->format('d-m-Y') }}</span> au <span class="badge badge-success">{{ Carbon\Carbon::parse($s->dateFin)->format('d-m-Y') }}</span>
                  </button>
                </h2>
              </div>
              <div id="o{{ $s->id }}" class="collapse" data-parent="#domaines">
                <div class="card-body">
                  <div class="row text-center">
                    <div class="col-md-12">
                      <p><i class="fas fa-graduation-cap"></i> Nom de la formation : <span class="badge badge-danger">{{ $p->nom }}</span></p>
                    </div>
                    <div class="col-md-12">
                      <p>{{ $p->description }}</p>
                    </div>
                    <div class="col-md-12">
                      <p><i class="far fa-calendar-alt"></i> Date de stage : Du <span class="badge badge-success"> {{ Carbon\Carbon::parse($s->dateDebut)->format('d-m-Y') }}</span> au <span class="badge badge-success"> {{ Carbon\Carbon::parse($s->dateFin)->format('d-m-Y') }}</span></p>
                    </div>
                    <div class="col-md-12">
                      <a class="btn btn-success btn-sm mb-2" href="{{ route('showFiche', $s->id) }}" role="button"><i class="far fa-edit"></i> Remplir la fiche de stage</a>
                      <a class="btn btn-info btn-sm mb-2" href="{{ route('contactPromotion', $p->id) }}" role="button"><i class="far fa-envelope"></i> Contacter le responsable</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @empty
              @empty($i)
                <p><i class="fas fa-exclamation-triangle"></i> Aucun stage n'est disponible actuellement pour ce domaine !</p>
              @endempty
            @endforelse
            @empty
            <p><i class="fas fa-exclamation-triangle"></i> Aucune promotion n'existe actuellement pour ce domaine !</p>
            @endforelse
          </div>
        </div>
      </div>
    </div>
  </section>
@stop
