@extends('template')

@section('title', 'Contacter le professeur principal')

@section('content')
<link rel="stylesheet" href="{{ asset('/css/landing-page.css') }}">

<style>
header.masthead {
    position: relative;
    background-color: #343a40;
    background: url({{ asset('storage/fiche.jpg') }}) no-repeat center center;
    background-size: cover;
    padding-top: 8rem;
    padding-bottom: 8rem;
}

input::-webkit-calendar-picker-indicator {
  position: absolute;
  width: 100%;
  height: 100%;
  opacity: 0;
}

@media (max-width: 768px) {
    .w-75 {
        width: 30%!important;
      }
    .captcha {
      padding-bottom: 1rem;
    }
}
</style>


<header class="masthead text-white text-center">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-xl-9 mx-auto">
        <h1 class="mb-5">Contacter le professeur principal</h1>
      </div>
    </div>
  </div>
</header>

<section class="testimonials text-center bg-light pt-5">
    <div class="container">

      <div class="d-flex justify-content-center" id="buttonBack">
        <a class="btn btn-danger" href="{{ URL::previous() }}" role="button"><i class="fas fa-arrow-circle-left"></i> Retour</a>
      </div>

      <div class="d-flex justify-content-center">
        @if ($errors->any())
        <div class="col-md-12 text-center p-3">
          <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <p style="margin-bottom: 0rem;"><i class="fas fa-times"></i> {{ $error }}</p>
            @endforeach
          </div>
        </div>
        @endif

        @if (session('status'))
        <div class="col-md-12 text-center p-3">
          <div class="alert alert-success" role="alert">
              <i class="fas fa-check"></i> {{ session('status') }}
          </div>
        </div>
        @endif
      </div>


      <form method="POST" action="{{ route('contactPP') }}">
        @csrf
        <input name="idPromotion" type="hidden" value="{{ $promotion->id }}">
        <input name="emailPP" type="hidden" value="{{ $promotion->mailresponsable }}">
        <div class="form-row p-3">
            <div class="col-md-3">
                <label>Identité</label>
            </div>
            <div class="col-md-9">
                <input type="text" placeholder="Exemple : Marie Dupont" class="form-control" name="identite" value="{{ old('identite') }}" required>
            </div>
        </div>
        <div class="form-row p-2">
            <div class="col-md-3">
                <label>Adresse mail</label>
            </div>
            <div class="col-md-9">
                <input type="email" placeholder="Exemple : contact@1mix.com" class="form-control" name="email" value="{{ old('email') }}" required>
            </div>
        </div>
        <div class="form-row p-2">
            <div class="col-md-3">
                <label>Votre message</label>
            </div>
            <div class="col-md-9">
                <textarea class="form-control" maxlength="1500" name="message" rows="5" required>{{ old('message') }}</textarea>
            </div>
        </div>
        <div class="form-row p-2">
            <div class="col-md-3">
                <label>Captcha</label>
            </div>
            <div class="col-md-6">
              <input type="text" class="form-control" name="captcha" minlength="6" maxlength="6" required>
            </div>
            <div class="col-md-3 captcha">
                <img class="w-75 img-thumbnail" src="{!! Captcha::src() !!}"></img>
            </div>
        </div>
        <button class="btn btn-success" type="submit"><i class="fas fa-paper-plane"></i> Envoyer</a>
        </form>

  </section>
@stop
