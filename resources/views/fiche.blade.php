@extends('template')

@section('title', 'Remplir la fiche de stage')

@section('content')
<link rel="stylesheet" href="{{ asset('/css/landing-page.css') }}">

<style>
header.masthead {
    position: relative;
    background-color: #343a40;
    background: url({{ asset('storage/fiche.jpg') }}) no-repeat center center;
    background-size: cover;
    padding-top: 8rem;
    padding-bottom: 8rem;
}

input::-webkit-calendar-picker-indicator {
  position: absolute;
  width: 100%;
  height: 100%;
  opacity: 0;
}

@media (max-width: 768px) {
    .w-75 {
        width: 30%!important;
      }
    .captcha {
      padding-bottom: 1rem;
    }
}
</style>

<script>
$(document).ready(function() {
  $("#step2").hide();
  $("#buttonStep").click(function(){
    $("#step1").hide();
    $("#step2").show();
    $("#buttonBack").hide();
  });
});
</script>

<header class="masthead text-white text-center">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-xl-9 mx-auto">
        <h1 class="mb-5">Remplir la fiche de stage</h1>
      </div>
    </div>
  </div>
</header>

<section class="testimonials text-center bg-light pt-5">
    <div class="container">

      <div class="d-flex justify-content-center" id="buttonBack">
        <a class="btn btn-danger" href="{{ URL::previous() }}" role="button"><i class="fas fa-arrow-circle-left"></i> Retour</a>
      </div>

      <div class="d-flex justify-content-center">
        @if ($errors->any())
        <script>
        $(document).ready(function() {
            $("#step1").show();
            $("#step2").show();
            $("#buttonStep").hide();
          });
        </script>

        <div class="col-md-12 text-center p-3">
          <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <p style="margin-bottom: 0rem;"><i class="fas fa-times"></i> {{ $error }}</p>
            @endforeach
          </div>
        </div>
        @endif
        @if (session('status'))
        <div class="col-md-12 text-center p-3">
          <div class="alert alert-success" role="alert">
              <i class="fas fa-check"></i> {{ session('status') }}
          </div>
        </div>
        @endif
      </div>

      <form method="POST" action="{{ route('insertFiche') }}">
        @csrf
        <input name="stage_id" type="hidden" value="{{ $stage->id }}">
        <div class="row p-3" id="step1">
          <div class="col-md-4">
              <div class="form-group">
                <label>Nom de l'élève</label>
                <input name="nom" type="text" class="form-control" placeholder="Exemple : Vuillemin" value="{{ old('nom') }}" required>
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                <label>Prénom de l'élève</label>
                <input name="prenom" type="text" class="form-control" placeholder="Exemple : Hugo" value="{{ old('prenom') }}" required>
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                <label>Classe</label>
                <input name="classe" type="text" class="form-control" placeholder="Exemple : BTS SIO 2 TP" value="{{ $stage->promotion->nom }}" required readonly>
              </div>
          </div>
          <div class="col-md-12">
              <div class="form-group">
                <label>Lieu du stage</label>
                <input name="lieuDuStage" type="text" class="form-control" placeholder="Exemple : 3 rue Crissey 39100 DOLE" value="{{ old('lieuDuStage') }}" required>
              </div>
          </div>
          <div class="col-md-6">
            <label>Date de début du stage</label>
            <div class="input-group mb-3">
              <input type="date" name="dateDebut" max="3000-12-31"min="1000-01-01" class="form-control" value="{{ $stage->dateDebut }}" readonly>
              <div class="input-group-append">
                <span class="input-group-text" id="datepickericon"><i class="far fa-calendar-alt"></i></span>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <label>Date de fin du stage</label>
            <div class="input-group mb-3">
              <input type="date" name="dateFin" max="3000-12-31"min="1000-01-01" class="form-control" value="{{ $stage->dateFin }}" readonly>
              <div class="input-group-append">
                <span class="input-group-text" id="datepickericon"><i class="far fa-calendar-alt"></i></span>
              </div>
            </div>
          </div>
          <div class="col-md-12 pb-3">
            <label>Horaires prevus durant le stage</label>
            <textarea class="form-control" name="horaires" rows="6" placeholder="Exemple : &#10;Lundi - 08:00 à 16h30, &#10;Mardi - 08:00 à 16h30, &#10;Mercredi - 08:00 à 16h30, &#10;Jeudi - 08:00 à 16h30, &#10;Vendredi - 08:00 à 16h30" required>{{ old('horaires') }}</textarea>
          </div>
          <div class="col-md-12 pb-3">
            <label>Activité durant le stage</label>
            <textarea class="form-control" name="activiteDurantLeStage" rows="3" placeholder="Exemple : Réparation et maintenance du parc informatique de l'entreprise." required>{{ old('activiteDurantLeStage') }}</textarea>
          </div>
          <div class="col-md-12">
            <button id="buttonStep" type="button" class="btn btn-success">Etape suivante <i class="fas fa-arrow-circle-right"></i></button>
          </div>
          </div>
          <div class="row p-3" id="step2">
          <div class="col-md-12">
              <div class="form-group">
                <label>Adresse de l'entreprise</label>
                <input name="adresse" type="text" class="form-control" placeholder="Exemple : 3 rue Crissey 39100 DOLE" value="{{ old('adresse') }}" required>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label>Raison sociale de l'entreprise</label>
                <input name="raisonSociale" type="text" class="form-control" placeholder="Exemple : SARL / SELAR / SELAS" value="{{ old('raisonSociale') }}" required>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label>Téléphone de l'entreprise</label>
                <input name="telephone" type="text" class="form-control" placeholder="Exemple : 03 84 69 99 99" value="{{ old('telephone') }}" required>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label>Fax de l'entreprise</label>
                <input name="fax" type="text" class="form-control" placeholder="Exemple : 03 84 69 99 98" value="{{ old('fax') }}" required>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label>Nombre de salariés</label>
                <input name="nombreDeSalaries" type="text" class="form-control" placeholder="Exemple : 11" value="{{ old('nombreDeSalaries') }}" required>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label>Email de l'entreprise</label>
                <input name="emailDeEntreprise" type="email" class="form-control" placeholder="Exemple : contact@1mix.com" value="{{ old('emailDeEntreprise') }}" required>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label>Code APE</label>
                <input name="codeAPE" type="text" class="form-control" placeholder="Exemple : 0143Z" value="{{ old('codeAPE') }}" required>
              </div>
          </div>
          <div class="col-md-12">
              <div class="form-group">
                <label>Descriptif de l'activité</label>
                <input name="descriptifDeActivite" type="text" class="form-control" placeholder="Exemple : Blanchisserie-teinturerie de détail" value="{{ old('descriptifDeActivite') }}" required>
              </div>
          </div>
          <div class="col-md-12">
              <div class="form-group">
                <label>Adresse du siège social</label>
                <input name="adresseDuSiegeSocial" type="text" class="form-control" placeholder="Exemple : 3 rue Crissey 39100 DOLE" value="{{ old('adresseDuSiegeSocial') }}" required>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label>Numéro de SIRET</label>
                <input name="siret" type="text" class="form-control" placeholder="Exemple : 362 521 879 00034" value="{{ old('siret') }}" required>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label>Identité du dirigeant</label>
                <input name="identiteDuDirigeant" type="text" class="form-control" placeholder="Exemple : Patrick Montclar" value="{{ old('identiteDuDirigeant') }}" required>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label>Identité du reponsable du personnel</label>
                <input name="identiteDuResponsableDePersonnel" type="text" class="form-control" placeholder="Exemple : Yannick Baccide" value="{{ old('identiteDuResponsableDePersonnel') }}" required>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label>Nom et fonction du reponsable de stage</label>
                <input name="nomEtFonctionDuResponsableDeStage" type="text" class="form-control" placeholder="Exemple : Thomas Dove - Ressource Humaine" value="{{ old('nomEtFonctionDuResponsableDeStage') }}" required>
              </div>
          </div>
          <div class="col-md-8">
              <div class="form-group">
                <label>Captcha</label>
                <input name="captcha" type="text" class="form-control" minlength="6" maxlength="6" required>
              </div>
          </div>
          <div class="col-md-4 captcha">
            <img class="img-thumbnail img-fluid w-75" src="{!! Captcha::src() !!}"></img>
          </div>
          <div class="col-md-12">
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Valider la fiche de stage</button>
          </div>
        </div>
      </form>
  </section>
@stop
