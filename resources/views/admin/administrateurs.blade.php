@extends('layouts.app')

@section('content')

@foreach($administrateurs as $a)

@if($a->superAdmin == 0)
<div class="modal fade" id="s{{ $a->id }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="s{{ $a->id }}">Suppression de l'administrateur n°{{ $a->id }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body text-center">
      <p>Êtes-vous sur de vouloir supprimer l'administrateur « {{ $a->name }} » ?</p>
      <div class="col">
        <form action="{{ route('deleteAdministrateurs') }}" method="POST">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        @csrf
        @method('DELETE')
        <input type="hidden" name="id" value="{{ $a->id }}">
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
@endif

<div class="modal fade" id="m{{ $a->id }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="m{{ $a->id }}">Modification de l'administrateur {{ $a->name }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form action="{{ route('updateAdministrateurs') }}" method="POST">
      @csrf
      @method('PUT')
      <input type="hidden" name="id" value="{{ $a->id }}">
      <div class="form-group">
        <label>Identité de l'administrateur</label>
        <input type="text" name="identité" class="form-control" value="{{ $a->name }}" required>
      </div>
      <div class="form-group">
        <label>Email de l'administrateur</label>
        <input type="email" name="email" class="form-control" value="{{ $a->email }}" required>
      </div>
      <div class="col text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
@endforeach

<div class="modal fade" id="ajouterAdministrateur" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="ajouterAdministrateur">Ajouter un administrateur</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form action="{{ route('insertAdministrateurs') }}" method="POST">
      @csrf
      <div class="form-group">
        <label>Identité de l'administrateur</label>
        <input type="text" name="identité" class="form-control" placeholder="Exemple : Sébastien Pernelle" required>
      </div>
      <div class="form-group">
        <label>Email de l'administrateur</label>
        <input type="email" name="email" class="form-control" placeholder="Exemple : spernelle@gmail.com" required>
      </div>
      <div class="form-group">
        <label>Mot de passe de l'administrateur</label>
        <input type="password" name="motDePasse" class="form-control" placeholder="Exemple : 1TvmQ2tl" required>
      </div>
      <div class="form-group">
        <label>Confirmation du mot de passe de l'administrateur</label>
        <input type="password" name="confirmationDeMotDePasse" class="form-control" placeholder="Exemple : 1TvmQ2tl" required>
      </div>
      <div class="col text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>

<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12 text-center" style="padding: 10px;">
        <h3>Gestion des administrateurs</h3>
      </div>

      @if ($errors->any())
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-danger" role="alert">
          @foreach ($errors->all() as $error)
              <p style="margin-bottom: 0rem;"><i class="fas fa-times"></i> {{ $error }}</p>
          @endforeach
        </div>
      </div>
      @endif

      @if (session('status'))
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-success" role="alert">
            <i class="fas fa-check"></i> {{ session('status') }}
        </div>
      </div>
      @endif

      <div class="d-flex justify-content-center">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ajouterAdministrateur"><i class="far fa-plus-square"></i>  Ajouter un administrateur</button>
      </div>

      <div class="col-md-12" style="padding: 10px;">
        <div class="table-responsive">
          <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Modifier</th>
                <th scope="col">Identité</th>
                <th scope="col">Email</th>
                <th scope="col">Supprimer</th>
              </tr>
            </thead>
            <tbody>
              @foreach($administrateurs as $a)
              <tr>
                <td><button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#m{{ $a->id }}"><i class="far fa-edit"></i> Modifier</button></td>
                <td>{{ $a->name }}</td>
                <td>{{ $a->email }}</td>
                @if($a->superAdmin == 1)
                <td><button type="button" class="btn btn-dark btn-sm" data-toggle="modal" data-target="#s{{ $a->id }}" disabled><i class="fas fa-trash"></i> Supprimer</button></td>
                @else
                <td><button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#s{{ $a->id }}"><i class="fas fa-trash"></i> Supprimer</button></td>
                @endif
              </tr>
              @endforeach
            </tbody>
          </table>
      </div>
      </div>
      <div class="d-flex justify-content-center">
        {{ $administrateurs->links() }}
      </div>
    </div>

@endsection
