@extends('layouts.app')

@section('content')

@foreach($promotions as $p)

<div class="modal fade" id="s{{ $p->id }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="s{{ $p->id }}">Suppression de la promotion {{ $p->nom }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body text-center">
      <p>Êtes-vous sur de vouloir supprimer la promotion {{ $p->nom }} et tous les stages associés ?</p>
      <div class="col">
        <form action="{{ route('deletePromotions') }}" method="POST">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        @csrf
        @method('DELETE')
        <input type="hidden" name="id" value="{{ $p->id }}">
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

<script src='{{asset("ckeditor/ckeditor.js")}}'></script>
<script>CKEDITOR.replaceClass="description";</script>

<div class="modal fade" id="m{{ $p->id }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="m{{ $p->id }}">Modification de la promotion des {{ $p->nom }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form action="{{ route('updatePromotions') }}" method="POST">
      @csrf
      @method('PUT')
      <input type="hidden" name="id" value="{{ $p->id }}">
      <div class="form-group">
        <label>Nom de la promotion</label>
        <input type="text" name="nom" class="form-control" value="{{ $p->nom }}" required>
      </div>
      <div class="form-group">
        <label>Description de la promotion</label>
        <textarea class="form-control description" name="description" maxlength="255" rows="4" required>{{ $p->description }}</textarea>
      </div>
      <div class="form-group">
        <label>Adresse e-mail du responsable</label>
        <input type="text" name="mail" class="form-control" value="{{ $p->mailresponsable }}" required>
      </div>
      <div class="form-group">
        <label>Domaine de la promotion</label>
        {{ Form::select('domaine', $domainesPluck, $p->domaine_id, ['class' => 'form-control']) }}
      </div>
      <div class="col text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>



<div class="modal fade" id="stage{{ $p->id }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="stage{{ $p->id }}">Ajouter un stage au {{ $p->nom }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form action="{{ route('insertStages') }}" method="POST">
      @csrf
      <div class="row">
        <div class="col-md-6">
          <label>Date de début du stage</label>
          <div class="input-group mb-3">
            <input type="date" name="dateDebut" max="3000-12-31"min="1000-01-01" class="form-control">
            <div class="input-group-append">
              <span class="input-group-text" id="datepickericon"><i class="far fa-calendar-alt"></i></span>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <label>Date de fin du stage</label>
          <div class="input-group mb-3">
            <input type="date" name="dateFin" max="3000-12-31"min="1000-01-01" class="form-control">
            <div class="input-group-append">
              <span class="input-group-text" id="datepickericon"><i class="far fa-calendar-alt"></i></span>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Description du stage</label>
            <input type="text" name="description" class="form-control" placeholder="Exemple : Stages de 6 semaines au sein d'une entreprise" required>
          </div>
        </div>
      </div>
      <input name="promotion" type="hidden" value="{{ $p->id }}">
      <div class="col text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
@endforeach

<div class="modal fade" id="ajouterPromotion" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="ajouterPromotion">Ajouter une promotion</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form action="{{ route('insertPromotions') }}" method="POST">
      @csrf
      <div class="form-group">
        <label>Nom de la promotion</label>
        <input type="text" name="nom" class="form-control" placeholder="Exemple : Bac Pro MEI" required>
      </div>
      <div class="form-group">
        <label>Description de la promotion</label>
        <textarea class="form-control" id="description" name="description" maxlength="255" rows="4" placeholder="Exemple : Bac qui permet de faire de la maintenance sur des équipements industriels" required></textarea>
      </div>
      <div class="form-group">
        <label>Adresse e-mail du responsable</label>
        <input type="email" name="mail" class="form-control" placeholder="Exemple : spernelle@gmail.com" required>
      </div>
      <div class="form-group">
        <label>Domaine de la promotion</label>
        <select class="form-control" name="domaine" required>
        <option disabled selected value>Séléctionner un domaine</option>
          @foreach($domaines as $d)
            <option value="{{ $d->id }}">{{ $d->nom }}</option>
          @endforeach
        </select>
      </div>
      <div class="col text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
<script src='{{asset("ckeditor/ckeditor.js")}}'></script>
<script>CKEDITOR.replace('description');</script>

<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12 text-center" style="padding: 10px;">
        <h3>Gestion des promotions</h3>
      </div>

      @if ($errors->any())
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-danger" role="alert">
          @foreach ($errors->all() as $error)
              <p style="margin-bottom: 0rem;"><i class="fas fa-times"></i> {{ $error }}</p>
          @endforeach
        </div>
      </div>
      @endif

      @if (session('status'))
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-success" role="alert">
            <i class="fas fa-check"></i> {{ session('status') }}
        </div>
      </div>
      @endif

      <div class="d-flex justify-content-center">
        <div class="col-md-12 text-center">
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ajouterPromotion"><i class="far fa-plus-square"></i>  Ajouter une promotion</button>
        </div>
      </div>

      <div class="col-md-12 pt-3 text-center">
        <form action="{{ route('searchPromotions') }}" method="POST">
          @csrf
          <div class="input-group mb-3">
            <input type="text" name="recherche" class="form-control" placeholder="Exemple : BTS SIO / Bac qui permet de de faire de la maintenance sur des équipements industriels" required>
            <div class="input-group-append">
              <button class="btn btn-success" type="submit"><i class="fas fa-search"></i> Rechercher</button>
            </div>
          </div>
        </form>
      </div>

      <div class="col-md-12" style="padding: 10px;">
        <div class="table-responsive">
          <table class="table" id="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Modifier</th>
                <th scope="col">Nom</th>
                <th scope="col">Description</th>
                <th scope="col">Mail du responsable</th>
                <th scope="col">Domaine</th>
                <th scope="col">Supprimer</th>
                <th scope="col">Ajouter</th>
              </tr>
            </thead>
            <tbody>
              @forelse($promotions as $p)
              <tr>
                <td><button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#m{{ $p->id }}"><i class="far fa-edit"></i> Modifier</button></td>
                <td>{{ $p->nom }}</td>
                <td>{{ $p->description }}</td>
                <td>{{ $p->mailresponsable }}</td>
                <td>{{ $p->domaine->nom }}</td>
                <td><button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#s{{ $p->id }}"><i class="fas fa-trash"></i> Supprimer</button></td>
                <td><button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#stage{{ $p->id }}"><i class="far fa-plus-square"></i> Ajouter un stage</button></td>
              </tr>
              @empty
              <div class="d-flex justify-content-center">
                <div class="col-md-12 text-center">
                  <style>#table { display:none; }</style>
                  <p>Aucun résultat trouvé.</p>
                </div>
              </div>
              @endforelse
            </tbody>
          </table>
      </div>
      </div>
      <div class="d-flex justify-content-center">
        {{ $promotions->links() }}
      </div>
    </div>

@endsection
