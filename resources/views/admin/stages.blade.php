@extends('layouts.app')

@section('content')
@foreach($stages as $s)
<div class="modal fade" id="s{{ $s->id }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="s{{ $s->id }}">Suppression du stage des {{ $s->promotion->nom }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body text-center">
      <p>Êtes-vous sur de vouloir supprimer le stage des {{ $s->promotion->nom }} ?</p>
      <div class="col">
        <form action="{{ route('deleteStages') }}" method="POST">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        @csrf
        @method('DELETE')
        <input type="hidden" name="id" value="{{ $s->id }}">
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="m{{ $s->id }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="m{{ $s->id }}">Modification du stage des {{ $s->promotion->nom }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form action="{{ route('updateStages') }}" method="POST">
      @csrf
      @method('PUT')
      <input type="hidden" name="id" value="{{ $s->id }}">
      <div class="row">
        <div class="col-md-6">
          <label>Date de début du stage</label>
          <div class="input-group mb-3">
            <input type="date" name="dateDebut" max="3000-12-31"min="1000-01-01" value="{{ Carbon\Carbon::parse($s->dateDebut)->format('Y-m-d') }}" class="form-control">
            <div class="input-group-append">
              <span class="input-group-text" id="datepickericon"><i class="far fa-calendar-alt"></i></span>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <label>Date de fin du stage</label>
          <div class="input-group mb-3">
            <input type="date" name="dateFin" max="3000-12-31"min="1000-01-01" value="{{ Carbon\Carbon::parse($s->dateFin)->format('Y-m-d') }}" class="form-control">
            <div class="input-group-append">
              <span class="input-group-text" id="datepickericon"><i class="far fa-calendar-alt"></i></span>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Description du stage</label>
            <input type="text" name="description" class="form-control" value="{{ $s->description }}" required>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Promotion du stage</label>
            {{ Form::select('promotion', $promotionsPluck, $s->promotion_id, ['class' => 'form-control']) }}
          </div>
        </div>
      </div>
      <div class="col text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
@endforeach

<div class="modal fade" id="ajouterStage" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="ajouterStage">Ajouter un stage</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form action="{{ route('insertStages') }}" method="POST">
      @csrf
      <div class="row">
        <div class="col-md-6">
          <label>Date de début du stage</label>
          <div class="input-group mb-3">
            <input type="date" name="dateDebut" max="3000-12-31"min="1000-01-01" class="form-control">
            <div class="input-group-append">
              <span class="input-group-text" id="datepickericon"><i class="far fa-calendar-alt"></i></span>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <label>Date de fin du stage</label>
          <div class="input-group mb-3">
            <input type="date" name="dateFin" max="3000-12-31"min="1000-01-01" class="form-control">
            <div class="input-group-append">
              <span class="input-group-text" id="datepickericon"><i class="far fa-calendar-alt"></i></span>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Description du stage</label>
            <input type="text" name="description" class="form-control" placeholder="Exemple : Stages de 6 semaines au sein d'une entreprise" required>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Promotion du stage</label>
            <select class="form-control" name="promotion" required>
            <option disabled selected value>Séléctionner une promotion</option>
              @foreach($promotions as $p)
                <option value="{{ $p->id }}">{{ $p->nom }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="col text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>

<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12 text-center" style="padding: 10px;">
        <h3>Gestion des stages</h3>
      </div>

      @if ($errors->any())
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-danger" role="alert">
          @foreach ($errors->all() as $error)
              <p style="margin-bottom: 0rem;"><i class="fas fa-times"></i> {{ $error }}</p>
          @endforeach
        </div>
      </div>
      @endif

      @if (session('status'))
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-success" role="alert">
            <i class="fas fa-check"></i> {{ session('status') }}
        </div>
      </div>
      @endif

      <div class="d-flex justify-content-center">
        <div class="col-md-12 text-center">
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ajouterStage"><i class="far fa-plus-square"></i>  Ajouter un stage</button>
        </div>
      </div>

      <div class="col-md-12 pt-3 text-center">
        <form action="{{ route('searchStages') }}" method="POST">
          @csrf
          <div class="input-group mb-3">
            <input type="text" name="recherche" class="form-control" placeholder="Exemple : 27-05-2020 / BTS SIO / Stages de 6 semaines au sein d'une entreprise" required>
            <div class="input-group-append">
              <button class="btn btn-success" type="submit"><i class="fas fa-search"></i> Rechercher</button>
            </div>
          </div>
        </form>
      </div>

      <div class="col-md-12" style="padding: 10px;">
        <div class="table-responsive">
          <table class="table" id="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Modifier</th>
                <th scope="col">Début</th>
                <th scope="col">Fin</th>
                <th scope="col">Description</th>
                <th scope="col">Promotion</th>
                <th scope="col">Supprimer</th>
              </tr>
            </thead>
            <tbody>
              @forelse($stages as $s)
              <tr>
                <td><button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#m{{ $s->id }}"><i class="far fa-edit"></i> Modifier</button></td>
                <td>{{ Carbon\Carbon::parse($s->dateDebut)->format('d-m-Y') }}</td>
                <td>{{ Carbon\Carbon::parse($s->dateFin)->format('d-m-Y') }}</td>
                <td>{{ $s->description }}</td>
                <td>{{ $s->promotion->nom }}</td>
                <td><button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#s{{ $s->id }}"><i class="fas fa-trash"></i> Supprimer</button></td>
              </tr>
              @empty
              <div class="d-flex justify-content-center">
                <div class="col-md-12 text-center">
                  <style>#table { display:none; }</style>
                  <p>Aucun résultat trouvé.</p>
                </div>
              </div>
              @endforelse
            </tbody>
          </table>
      </div>
      </div>
      <div class="d-flex justify-content-center">
        {{ $stages->links() }}
      </div>
    </div>

@endsection
