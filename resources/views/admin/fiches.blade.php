@extends('layouts.app')

@section('content')

@foreach($fiches as $f)
<div class="modal fade" id="etat{{ $f->id }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="etat{{ $f->id }}">Modification de l'état de la fiche</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body text-center">
      <form action="{{ route('updateFiches') }}" method="POST">
      @csrf
      @method('PUT')
      <input type="hidden" name="id" value="{{ $f->id }}">
      <div class="form-group">
        <label>État de la fiche</label>
        {{ Form::select('etat', $etatPluck, $f->etat_id, ['class' => 'form-control']) }}
      </div>
      <div class="col text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
@endforeach

<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12 text-center" style="padding: 10px;">
        <h3>Gestion des fiches</h3>
      </div>

      @if ($errors->any())
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-danger" role="alert">
          @foreach ($errors->all() as $error)
              <p style="margin-bottom: 0rem;"><i class="fas fa-times"></i> {{ $error }}</p>
          @endforeach
        </div>
      </div>
      @endif

      @if (session('status'))
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-success" role="alert">
            <i class="fas fa-check"></i> {{ session('status') }}
        </div>
      </div>
      @endif

      <div class="col-md-12 pt-3 text-center">
        <form action="{{ route('searchFiches') }}" method="POST">
          @csrf
          <div class="input-group mb-3">
            <input type="text" name="recherche" class="form-control" placeholder="Exemple : Thomas / Guinchard / SIO 2 / Date de début / Date de fin / Lieu du stage" required>
            <div class="input-group-append">
              <button class="btn btn-success" type="submit"><i class="fas fa-search"></i> Rechercher</button>
            </div>
          </div>
        </form>
      </div>

      <div class="col-md-12" style="padding: 10px;">
        <div class="table-responsive">
          <table class="table" id="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Statut</th>
                <th scope="col">Élève</th>
                <th scope="col">Classe</th>
                <th scope="col">Date de stage</th>
                <th scope="col">Lieu du stage</th>
                <th scope="col">Détails</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @forelse($fiches as $f)
              <tr>
                <td><button type="button" class="btn btn-{{ $f->etat->couleur }} btn-sm">{{ $f->etat->libelle }}</button></td>
                <td>{{ $f->nom }} {{ $f->prenom }}</td>
                <td>{{ $f->classe }}</td>
                <td>{{ Carbon\Carbon::parse($f->datedebutstage)->format('d-m-Y') }} au {{ Carbon\Carbon::parse($f->datefinstage)->format('d-m-Y') }}</td>
                <td>{{ $f->lieu }}</td>
                <td><a target="_blank" class="btn btn-info btn-sm" href="{{ asset('pdf/pdf'.$f->id.'.pdf') }}" role="button"><i class="far fa-eye"></i> Plus d'informations</a></td>
                <td><button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#etat{{ $f->id }}"><i class="far fa-edit"></i> Modifier l'état de la fiche</button></td>
              </tr>
              @empty
              <div class="d-flex justify-content-center">
                <div class="col-md-12 text-center">
                  <style>#table { display:none; }</style>
                  <p>Aucun résultat trouvé.</p>
                </div>
              </div>
              @endforelse
            </tbody>
          </table>
      </div>
      </div>
      <div class="d-flex justify-content-center">
        {{ $fiches->links() }}
      </div>
    </div>

@endsection
