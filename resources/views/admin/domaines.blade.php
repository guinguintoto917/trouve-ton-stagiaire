@extends('layouts.app')

@section('content')
<script>
    $(document).ready(function(){
        $('input[name="image"]').change(function(e){
            var fileName = e.target.files[0].name;
            document.getElementById('image').innerHTML = fileName;
        });
    });
    $(document).ready(function(){
        $('input[name="logo"]').change(function(e){
            var fileName = e.target.files[0].name;
            document.getElementById('logo').innerHTML = fileName;
        });
    });
    $(document).ready(function(){
        $('input[name="logoDomaine"]').change(function(e){
            var fileName = e.target.files[0].name;
            document.getElementById('logoDomaine').innerHTML = fileName;
        });
    });
    $(document).ready(function(){
        $('input[name="imageDomaine"]').change(function(e){
            var fileName = e.target.files[0].name;
            document.getElementById('imageDomaine').innerHTML = fileName;
        });
    });
</script>


@foreach($domaines as $d)
<div class="modal fade" id="s{{ $d->id }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="s{{ $d->id }}">Suppression du domaine n°{{ $d->id }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body text-center">
      <p>Êtes-vous sur de vouloir supprimer le domaine « {{ $d->nom }} » et toutes les promotions associées et tous les stages associés à celle-ci ?</p>
      <div class="col">
        <form action="{{ route('deleteDomaines') }}" method="POST">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        @csrf
        @method('DELETE')
        <input type="hidden" name="id" value="{{ $d->id }}">
        <input type="hidden" name="logo" value="{{ $d->logo }}">
        <input type="hidden" name="image" value="{{ $d->image }}">
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="m{{ $d->id }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="m{{ $d->id }}">Modification du domaine n°{{ $d->id }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form action="{{ route('updateDomaines') }}" enctype="multipart/form-data" method="POST">
      @csrf
      @method('PUT')
      <input type="hidden" name="id" value="{{ $d->id }}">
      <div class="form-group">
        <label>Nom du domaine</label>
        <input type="text" name="nom" class="form-control" value="{{ $d->nom }}" required>
      </div>
      <div class="form-group">
        <label>Description du domaine</label>
        <textarea class="form-control" name="description" maxlength="255" rows="4" required>{{ $d->description }}</textarea>
      </div>
      <div class="form-group">
        <label>Logo du domaine <a target="_blank" href="{{ asset('/') }}storage/{{ $d->logo }}">(<i class="far fa-images"></i> Voir le logo actuel)</a></label>
        <div class="custom-file">
          <input accept='image/*' name="logo" type="file" class="custom-file-input" lang="fr">
          <label id="logo" class="custom-file-label">{{ $d->logo }}</label>
        </div>
      </div>
      <div class="form-group">
        <label>Arrière plan du domaine <a target="_blank" href="{{ asset('/') }}storage/{{ $d->image }}">(<i class="far fa-images"></i> Voir l'arrière plan actuel)</a></label>
        <div class="custom-file">
          <input accept='image/*' name="image" type="file" class="custom-file-input" lang="fr">
          <label id="image" class="custom-file-label">{{ $d->image }}</label>
        </div>
      </div>
      <div class="col text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
@endforeach

<div class="modal fade" id="ajouterDomaine" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="ajouterDomaine">Ajouter un domaine</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form action="{{ route('insertDomaines') }}" enctype="multipart/form-data" method="POST">
      @csrf
      <div class="form-group">
        <label>Nom du domaine</label>
        <input type="text" name="nom" class="form-control" placeholder="Exemple : Métiers de la Santé & du Social" required>
      </div>
      <div class="form-group">
        <label>Description du domaine</label>
        <textarea class="form-control" id="description" name="description" maxlength="255" rows="4" placeholder="Exemple : Ce domaine regoupe plusieurs formations telles que les BTS ESF (Economie, Sociale & Familiale) et les BTS SP3S (Services et Prestation des Secteurs Sanitaire et Social)" required></textarea>
      </div>
      <div class="form-group">
        <label>Logo du domaine</label>
        <div class="custom-file">
          <input accept='image/*' name="logoDomaine" type="file" class="custom-file-input" lang="fr" required>
          <label id="logoDomaine" class="custom-file-label" for="validatedCustomFile">Choisir le logo..</label>
        </div>
      </div>
      <div class="form-group">
        <label>Arrière plan du domaine</label>
        <div class="custom-file">
          <input accept='image/*' name="imageDomaine" type="file" class="custom-file-input" lang="fr" required>
          <label id="imageDomaine" class="custom-file-label" for="validatedCustomFile">Choisir l'arrière plan..</label>
        </div>
      </div>
      <div class="col text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Annuler</button>
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>

<script src='{{asset("ckeditor/ckeditor.js")}}'></script>
<script>CKEDITOR.replace('description');</script>

<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12 text-center" style="padding: 10px;">
        <h3>Gestion des domaines</h3>
      </div>

      @if ($errors->any())
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-danger" role="alert">
          @foreach ($errors->all() as $error)
              <p style="margin-bottom: 0rem;"><i class="fas fa-times"></i> {{ $error }}</p>
          @endforeach
        </div>
      </div>
      @endif

      @if (session('status'))
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-success" role="alert">
            <i class="fas fa-check"></i> {{ session('status') }}
        </div>
      </div>
      @endif

      <div class="d-flex justify-content-center">
        <div class="col-md-12 text-center">
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ajouterDomaine"><i class="far fa-plus-square"></i>  Ajouter un domaine</button>
        </div>
      </div>

      <div class="col-md-12 pt-3 text-center">
        <form action="{{ route('searchDomaines') }}" method="POST">
          @csrf
          <div class="input-group mb-3">
            <input type="text" name="recherche" class="form-control" placeholder="Exemple : Métiers de l'Industrie & des Nouvelles Technologies" required>
            <div class="input-group-append">
              <button class="btn btn-success" type="submit"><i class="fas fa-search"></i> Rechercher</button>
            </div>
          </div>
        </form>
      </div>

      <div class="col-md-12" style="padding: 10px;">
        <div class="table-responsive">
          <table class="table" id="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Modifier</th>
                <th scope="col">Date</th>
                <th scope="col">Description</th>
                <th scope="col">Supprimer</th>
              </tr>
            </thead>
            <tbody>
              @forelse($domaines as $d)
              <tr>
                <td><button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#m{{ $d->id }}"><i class="far fa-edit"></i> Modifier</button></td>
                <td>{{ $d->nom }}</td>
                <td>{{ $d->description }}</td>
                <td><button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#s{{ $d->id }}"><i class="fas fa-trash"></i> Supprimer</button></td>
              </tr>
              @empty
              <div class="d-flex justify-content-center">
                <div class="col-md-12 text-center">
                  <style>#table { display:none; }</style>
                  <p>Aucun résultat trouvé.</p>
                </div>
              </div>
              @endforelse
            </tbody>
          </table>
      </div>
      </div>
      <div class="d-flex justify-content-center">
        {{ $domaines->links() }}
      </div>
    </div>

@endsection
