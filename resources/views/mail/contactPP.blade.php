<style>
    /* CLIENT-SPECIFIC STYLES */
    #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
    .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
    body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* RESET STYLES */
    body{margin:0; padding:0;}
    img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
    table{border-collapse:collapse !important;}
    body{height:100% !important; margin:0; padding:0; width:100% !important;}

    .appleBody a {color:#68440a; text-decoration: none;}
    .appleFooter a {color:#999999; text-decoration: none;}

            a[class="article-link"]{
                font-weight: bold !important;
            }

            a:hover[class="article-link"]{
                color: #2b2b2b !important;
                border-bottom: 1px dotted #2b2b2b !important;
            }

            a:visited[class="article-link"]{
                color: #2b2b2b !important;
                border-bottom: 1px dotted #2b2b2b !important;
            }

    /* MOBILE STYLES */
    @media screen and (max-width: 525px) {

        /* ALLOWS FOR FLUID TABLES */
        table[class="wrapper"]{
          width:100% !important;
        }

        /* FULL-WIDTH TABLES */
        table[class="responsive-table"]{
          width:100%!important;
        }

        /* ADJUST BUTTONS ON MOBILE */
        td[class="mobile-wrapper"]{
            padding: 30px 5% 30px 5% !important;
        }

        a[class="mobile-button"]{
            width:85% !important;
            padding: 15px !important;
            border: 0 !important;
            font-size: 16px !important;
        }

        /* CUSTOM MOBILE STYLING */

            /* CARD STYLING */
            td[class="padding-logo"]{
              padding: 5% 10% 5% 10% !important;
              text-align: left !important;
              font-size: 18px !important;
            }

            td[class="padding-headline"]{
              padding: 10% 10% 0 10% !important;
              text-align: left !important;
              font-size: 18px !important;
            }

            td[class="padding-copy"]{
              padding: 5% 10% 10% 10% !important;
              text-align: left !important;
              font-size: 14px !important;
              line-height: 26px !important;
            }

            td[class="button-padding-copy"]{
              padding: 2% 0 2% 0 !important;
              text-align: center;
            }

            td[class="button-padding"]{
              padding: 0% 10% 10% 10% !important;
              text-align: center;
            }

            a[class="article-link"]{
                font-weight: bold !important;
            }

            a:hover[class="article-link"]{
                color: #2b2b2b !important;
                border-bottom: 1px dotted #2b2b2b !important;
            }

            a:visited[class="article-link"]{
                color: #2b2b2b !important;
                border-bottom: 1px dotted #2b2b2b !important;
            }
    }
    </style>

    <body style="margin: 0; padding: 0;">

    <!-- NOTIFICATION -->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="center" bgcolor="#f1efef">
                <table border="0" cellpadding="0" cellspacing="0" width="580" class="wrapper">
                    <tr>
                        <td style="padding: 40px 0 40px 0;" class="mobile-wrapper">
                             <table border="0" cellpadding="0" cellspacing="0" width="580" class="wrapper">
                                <tr>
                                    <td bgcolor="#ffffff">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="responsive-table">
                                            <tr>
                                                <td bgcolor="#222222" width="100%" align="center">
                                                    <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <!-- LOGO -->
                                                        <tr>
                                                            <td align="left" style="padding: 20px 10px 20px 40px; color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 500; text-transform: uppercase;" class="padding-logo">
                                                                Trouver son stagiaire - Notification
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#ffffff" align="left" style="padding: 10px 40px 10px 40px; font-family: Helvetica, Arial, sans-serif; font-size: 14px; letter-spacing: .5px; font-weight: 100; color: #999999; line-height: 28px;" class="padding-copy">
                                                                Vous avez un nouveau message de {{ $identite }} !
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#ffffff" align="left" style="padding: 10px 40px 10px 40px; font-family: Helvetica, Arial, sans-serif; font-size: 14px; letter-spacing: .5px; font-weight: 100; color: #999999; line-height: 28px;" class="padding-copy">
                                                                Expéditeur : {{ $email }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#ffffff" align="left" style="padding: 10px 40px 10px 40px; font-family: Helvetica, Arial, sans-serif; font-size: 14px; letter-spacing: .5px; font-weight: 100; color: #999999; line-height: 28px;" class="padding-copy">
                                                                Message : {{ $bodyMessage }}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    </body>
