<?php

use Illuminate\Database\Seeder;

class EtatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('etats')->insert([
            'id' => "1",
            'libelle' => "En attente",
            'ordre' => "1",
            'couleur' => "secondary",
            'contenu' => "Votre fiche est en attende de validation par notre établissement. Vous serez tenu au courant de son avancement par mail.",
        ]);
        DB::table('etats')->insert([
            'id' => "2",
            'libelle' => "Validée",
            'ordre' => "2",
            'couleur' => "warning",
            'contenu' => "Votre fiche a été validée par notre établissement. Nous allons pouvoir éditer votre convention et vous la renvoyer dans
            les plus brefs délais.",
        ]);
        DB::table('etats')->insert([
            'id' => "3",
            'libelle' => "Convention envoyé",
            'ordre' => "3",
            'couleur' => "info",
            'contenu' => "Votre convention vous a été envoyée. Nous restons disponibles pour tout renseignements par rapport à celle-ci.",
        ]);
        DB::table('etats')->insert([
            'id' => "4",
            'libelle' => "Réception convention",
            'ordre' => "4",
            'couleur' => "success",
            'contenu' => "Votre convention signée a bien été reçue par notre établissement.",
        ]);
    }
}
