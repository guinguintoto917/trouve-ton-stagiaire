<?php

use Illuminate\Database\Seeder;

class FichesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Fiches')->insert([
        	'numFic' => bigIncrements('num'),
        	'nom' => string('nom'),
			'prenom' => string('prenom'),
			'classe' => string('classe'),
			'date' => string('date'),
			'lieu' => string('lieu'),
			'horaire' => string('horaire'),
		]);
    }
}
