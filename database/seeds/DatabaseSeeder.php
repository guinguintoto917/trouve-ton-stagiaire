<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(DomaineTableSeeder::class);
        $this->call(PromotionsTableSeeder::class);
        $this->call(StagesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(EtatsTableSeeder::class);
    }
}
