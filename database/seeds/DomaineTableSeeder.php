<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DomaineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('domaines')->insert([
        	'id' => "1",
        	'nom' => "Métiers de l'Industrie & des Nouvelles Technologies",
          'description' =>"Ce domaine regroupe plusieurs formations telles que les BTS SIO (Services Informatiques aux Organisations), les Bac Pro SN (Systèmes numériques) et les Bac Pro MEI (Maintenance des Equipements Industriels)",
          'image' => "technology.jpg",
          'logo' => "nouvelle-techno.png",
        ]);
        DB::table('domaines')->insert([
            'id' => "2",
            'nom' => "Métiers de la Santé & du Social",
            'description' => "Ce domaine regoupe plusieurs formations telles que les BTS ESF (Economie, Sociale & Familiale) et les BTS SP3S (Services et Prestation des Secteurs Sanitaire et Social",
            'image' => "healthy.jpg",
            'logo' => "santer-et-social.png",
        ]);
        DB::table('domaines')->insert([
            'id' => "3",
            'nom' => "Métiers des Arts & du Design",
            'description' => "Ce domaine regroupe plusieurs formations telles que les DN MADE Spectacle-Costume de scènes et les DTMS option technique de l'habillage",
            'image' => "design.jpg",
            'logo' => "arts-et-design.png",
        ]);
        DB::table('domaines')->insert([
            'id' => "4",
            'nom' => "Métiers du Commerce, du Management & de la Communication",
            'description' => "Ce domaine regroupe plusieurs formations telles que les BTS CG (Comptabilité Gestion) et les BTS SAM (Support à l'action managériale)",
            'image' => "commerce.jpg",
            'logo' => "shop.png",
        ]);
    }
}
