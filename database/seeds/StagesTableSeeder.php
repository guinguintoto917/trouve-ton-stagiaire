<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stages')->insert([
            'id' => "1",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 5 semaines au sein d'une entreprise",
            'promotion_id' => "1",
        ]);
        DB::table('stages')->insert([
            'id' => "2",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 6 semaines au sein d'une entreprise",
            'promotion_id' =>"2",
        ]);
        DB::table('stages')->insert([
            'id' => "3",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 5 semaines au sein d'une entreprise",
            'promotion_id' => "3",
        ]);
        DB::table('stages')->insert([
            'id' => "4",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 5 semaines au sein d'une entreprise",
            'promotion_id' => "4",
        ]);
        DB::table('stages')->insert([
            'id' => "5",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 5 semaines au sein d'une entreprise",
            'promotion_id' => "5",
        ]);
        DB::table('stages')->insert([
            'id' => "6",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 6 semaines au sein d'une entreprise",
            'promotion_id' => "6",
        ]);
        DB::table('stages')->insert([
            'id' => "7",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 5 semaines au sein d'une entreprise",
            'promotion_id' => "7",
        ]);
        DB::table('stages')->insert([
            'id' => "8",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 6 semaines au sein d'une entreprise",
            'promotion_id' => "8",
        ]);
        DB::table('stages')->insert([
            'id' => "9",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 5 semaines au sein d'une entreprise",
            'promotion_id' => "9",
        ]);
        DB::table('stages')->insert([
            'id' => "10",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 5 semaines au sein d'une entreprise",
            'promotion_id' => "10",
        ]);
        DB::table('stages')->insert([
            'id' => "11",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 5 semaines au sein d'une entreprise",
            'promotion_id' => "11",
        ]);
        DB::table('stages')->insert([
            'id' => "12",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 6 semaines au sein d'une entreprise",
            'promotion_id' => "12"
        ]);
        DB::table('stages')->insert([
            'id' => "13",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 5 semaines au sein d'une entreprise",
            'promotion_id' => "13",
        ]);
        DB::table('stages')->insert([
            'id' =>"14",
            'dateDebut' => "2020/05/27",
            'dateFin' => "2020/06/28",
            'description' => "Stages de 6 semaines au sein d'une entreprise",
            'promotion_id' => "14",
        ]);
    }
}
