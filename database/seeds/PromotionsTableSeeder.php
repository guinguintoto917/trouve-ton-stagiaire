<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PromotionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('promotions')->insert([
            'id' => "1",
            'nom' => "BTS SIO 1",
            'description' => "BTS qui se divise en 2 options SLAM et SISR",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "1",
        ]);
        DB::table('promotions')->insert([
            'id' => "2",
            'nom' => "BTS SIO 2",
            'description' => "BTS qui se divise en 2 options SLAM et SISR",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "1",
        ]);
        DB::table('promotions')->insert([
            'id' => "3",
            'nom' => "Bac Pro SN",
            'description' => "Bac qui permet de pouvoir intervenir sur des équipements électroniques",
            'mailresponsable' => "spernell@gmail.com",
            'domaine_id' => "1",
        ]);
        DB::table('promotions')->insert([
            'id' => "4",
            'nom' => "Bac Pro MEI",
            'description' => "Bac qui permet de  de faire de la maintenance sur des équipements industriels",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "1",
        ]);
        DB::table('promotions')->insert([
            'id' => "5",
            'nom' => "BTS ESF 1",
            'description' => "BTS qui permet de réaliser des missions pour les établissements qui l'emploient sur les domaines de la vie quotidienne",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "2"
        ]);
        DB::table('promotions')->insert([
            'id' => "6",
            'nom' => "BTS ESF 2",
            'description' => "BTS qui permet de réaliser des missions pour les établissements qui l'emploient sur les domaines de la vie quotidienne",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "2"
        ]);
        DB::table('promotions')->insert([
            'id' => "7",
            'nom' => "BTS SP3S 1",
            'description' => "BTS qui permet de conseiller des organismes de protection sociale",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "2",
        ]);
        DB::table('promotions')->insert([
            'id' => "8",
            'nom' => "BTS SP3S 2",
            'description' => "BTS qui permet de conseiller des organismes de protection sociale",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "2",
        ]);
        DB::table('promotions')->insert([
            'id' => "9",
            'nom' =>"DN MADE Spectacle - Costumes de Scène",
            'description' => "Ce diplôme permet de former les futurs concepteurs,réalisateur de costumes pour le spectacle",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "3",
        ]);
        DB::table('promotions')->insert([
            'id' => "10",
            'nom' => "DTMS option Technique de l'Habillage",
            'description' => "Ce diplôme permet de pouvoir aider à l'habillage et au maintien des costumes",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "3",
        ]);
        DB::table('promotions')->insert([
            'id' =>"11",
            'nom' => "BTS CG 1",
            'description' => "BTS qui permet de devenir comptable dans un cabinet ou dans un service comptabilité d'une entreprise",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "4",
        ]);
        DB::table('promotions')->insert([
            'id' =>"12",
            'nom' => "BTS CG 2",
            'description' => "BTS qui permet de devenir comptable dans un cabinet ou dans un service comptabilité d'une entreprise",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "4",
        ]);
        DB::table('promotions')->insert([
            'id' => "13",
            'nom' => "BTS SAM 1",
            'description' => "BTS qui permet d'améliorer les processus, la gestion des projets, des ressources humaines d'une entreprise",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "4",
        ]);
        DB::table('promotions')->insert([
            'id' => "14",
            'nom' => "BTS SAM 2",
            'description' => "BTS qui permet d'améliorer les processus, la gestion des projets, des ressources humaines d'une entreprise",
            'mailresponsable' => "spernelle@gmail.com",
            'domaine_id' => "4",
        ]);
    }
}
