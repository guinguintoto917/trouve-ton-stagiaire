<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFichesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fiches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->string('prenom');
            $table->string('classe');
            $table->string('lieu');
            $table->date('datedebutstage');
            $table->date('datefinstage');
            $table->string('horairesprevus');
            $table->string('adresse');
            $table->string('raisonsociale');
            $table->string('telephone');
            $table->string('fax');
            $table->string('nbsalaries');
            $table->string('mailentreprise');
            $table->string('codeape');
            $table->string('descriptifactivite');
            $table->string('activitestage');
            $table->string('adressesiege');
            $table->string('numsiret');
            $table->string('dirigeants');
            $table->string('nomresponsablepersonnel');
            $table->string('nomfonctionresponsablestage');
            $table->unsignedBigInteger('stage_id');
            $table->unsignedBigInteger('etat_id');
            $table->foreign('stage_id')->references('id')->on('stages');
            $table->foreign('etat_id')->references('id')->on('etats');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fiches');
    }
}
