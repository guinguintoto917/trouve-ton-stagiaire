<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    public function promotion()
    {
      return $this->belongsTo('App\Promotion');
    }
    public function fiche()
    {
      return $this->hasMany('App\Fiche');
    }
}
