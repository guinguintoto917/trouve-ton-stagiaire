<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    public function domaine()
    {
      return $this->belongsTo('App\Domaine');
    }

    public function stage()
    {
      return $this->hasMany('App\Stage');
    }
}
