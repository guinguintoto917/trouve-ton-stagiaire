<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promotion;
use App\Domaine;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotion = Promotion::all();
        return view('promotion.index')->with('promotion',$promotion);
    }

    public function contactPP($id)
    {
      $promotion = Promotion::findOrFail($id);
      return view('contactPP')->with('promotion',$promotion);
    }

    public function store(Request $request)
    {
        $promotion = new Promotion();
        $promotion->nom = $request->input('nom');
        $promotion->description = $request->input('description');
        $promotion->mailresponsable = $request->input('mailresponsable');
        $promotion->domaine_id = $request->input('domaine');
        $promotion->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promotion = Promotion::all()->where("domaine_id", "=", $id);
      return view('promotion.index')->with('promotion', $promotion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $promotion = Promotion::find($id);
      $promotion->nom = $request->input('nom');
      $promotion->description = $request->input('description');
      $promotion->mailresponsable = $request->inout('mailresponsable');
      $promotion->domaine_id = $request->input('domaine');
      $promotion->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
