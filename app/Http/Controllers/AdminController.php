<?php

namespace App\Http\Controllers;

use Hash;
use DateTime;
use App\Stage;
use App\Promotion;
use App\Domaine;
use App\User;
use App\Etat;
use App\Fiche;
use App\Mail\EtatMail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin');
    }

    // CRUD ADMIN FICHE :
    public function getAllFiches()
    {
      $fiches = Fiche::orderBy('id', 'desc')->paginate(10);
      $etatPluck = Etat::orderBy('id', 'ASC')->pluck('libelle', 'id');
      return view('admin.fiches')->with("fiches", $fiches)->with("etatPluck", $etatPluck);
    }

    public function updateFiches(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'id' => 'required',
        'etat' => 'required',
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $fiches = Fiche::find($request->id);
      $fiches->etat_id = $request->etat;
      $fiches->save();


      // CODE QUI ENVOIE UN MAIL A L'ENTREPRISE POUR DIRE QUE LA CONVENTION A ETE MODIFIER

      Mail::to($fiches->mailentreprise)->send(new EtatMail($fiches));

      return redirect('admin/fiches')->with('status', "L'état de la fiche a été modifié avec succès.");
    }

    public function searchFiches(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'recherche' => 'required',
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $fiches = Fiche::orderBy('nom', 'asc')->where('nom', 'LIKE', '%'.$request->recherche.'%')
                                            ->orWhere('prenom', 'LIKE', '%'.$request->recherche.'%')
                                            ->orWhere('classe', 'LIKE', '%'.$request->recherche.'%')
                                            ->orWhere('datedebutstage', 'LIKE', '%'.Carbon::parse($request->recherche)->format('Y-m-d').'%')
                                            ->orWhere('datefinstage', 'LIKE', '%'.Carbon::parse($request->recherche)->format('Y-m-d').'%')
                                            ->orWhere('lieu', 'LIKE', '%'.$request->recherche.'%')
                                            ->orderBy('id', 'desc')->paginate(10);

      $etatPluck = Etat::orderBy('id', 'ASC')->pluck('libelle', 'id');
      return view('admin.fiches')->with("fiches", $fiches)->with('etatPluck', $etatPluck);
    }

    // CRUD ADMIN STAGE :
    public function getAllStages()
    {
      $stages = Stage::orderBy('id', 'desc')->paginate(10);
      $promotions = Promotion::orderBy('nom', 'ASC')->get();
      $promotionsPluck = Promotion::orderBy('nom', 'ASC')->pluck('nom', 'id');
      return view('admin.stages')->with("stages", $stages)
                                 ->with("promotions", $promotions)
                                 ->with("promotionsPluck", $promotionsPluck);
    }

    public function searchStages(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'recherche' => 'required',
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      if (DateTime::createFromFormat('d-m-Y', $request->recherche) !== FALSE) {
        $stages = Stage::orderBy('id', 'desc')->where('description','LIKE','%'.$request->recherche.'%')->orWhere('dateDebut', 'LIKE','%'.Carbon::parse($request->recherche)->format('Y-m-d').'%')->orWhere('dateFin', 'LIKE','%'.Carbon::parse($request->recherche)->format('Y-m-d').'%')->orWhereHas('promotion', function($query) use ($request)
        { $query->where('nom', 'LIKE', '%'.$request->recherche.'%')->orWhere('description', 'LIKE', '%'.$request->recherche.'%'); })->paginate(10);
      } else {
        $stages = Stage::orderBy('id', 'desc')->where('description','LIKE','%'.$request->recherche.'%')->orWhereHas('promotion', function($query) use ($request)
        { $query->where('nom', 'LIKE', '%'.$request->recherche.'%')->orWhere('description', 'LIKE', '%'.$request->recherche.'%'); })->paginate(10);
      }

      $promotions = Promotion::orderBy('nom', 'ASC')->get();
      $promotionsPluck = Promotion::orderBy('nom', 'ASC')->pluck('nom', 'id');
      return view('admin.stages')->with("stages", $stages)
                                 ->with("promotions", $promotions)
                                 ->with("promotionsPluck", $promotionsPluck);
    }

    public function insertStages(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'dateDebut' => 'required|date',
        'dateFin' => 'required|date|after:dateDebut',
        'description' => 'required',
        'promotion' => 'required'
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $stages = new Stage();
      $stages->dateDebut = Carbon::parse($request->input('dateDebut'))->format('Y-m-d');
      $stages->dateFin = Carbon::parse($request->input('dateFin'))->format('Y-m-d');
      $stages->description = $request->input('description');
      $stages->promotion_id = $request->input('promotion');
      $stages->save();
      return redirect('admin/stages')->with('status', 'Le stage a été ajouté avec succès.');
    }

    public function updateStages(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'dateDebut' => 'required|date',
        'dateFin' => 'required|date',
        'description' => 'required',
        'promotion' => 'required'
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $stages = Stage::find($request->id);
      $stages->dateDebut = Carbon::parse($request->input('dateDebut'))->format('Y-m-d');
      $stages->dateFin = Carbon::parse($request->input('dateFin'))->format('Y-m-d');
      $stages->description = $request->input('description');
      $stages->promotion_id = $request->input('promotion');
      $stages->save();
      return redirect('admin/stages')->with('status', 'Le stage a été modifié avec succès.');
    }

    public function deleteStages(Request $request)
    {
      Stage::destroy($request->id);
      return redirect('admin/stages')->with('status', 'Le stage a été supprimé avec succès.');
    }

    // CRUD ADMIN DOMAINES :
    public function getAllDomaines()
    {
      $domaines = Domaine::orderBy('nom', 'asc')->paginate(10);
      return view('admin.domaines')->with("domaines", $domaines);
    }

    public function searchDomaines(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'recherche' => 'required',
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $domaines = Domaine::orderBy('nom', 'asc')->where('nom', 'LIKE', '%'.$request->recherche.'%')->orWhere('description', 'LIKE', '%'.$request->recherche.'%')->paginate(10);
      return view('admin.domaines')->with("domaines", $domaines);
    }

    public function insertDomaines(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'nom' => 'required',
        'description' => 'required',
        'imageDomaine' => 'mimes:jpeg,jpg,png|required|image',
        'logoDomaine' => 'mimes:jpeg,jpg,png|required|image',
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $timestamp = Carbon::now()->timestamp;
      Image::make($request->file('imageDomaine'))->resize(1200, 800)->encode('jpg')->save('storage/image'.$timestamp.'.jpg');
      Image::make($request->file('logoDomaine'))->resize(512, 512)->encode('png')->save('storage/logo'.$timestamp.'.png');
      $domaines = new Domaine();
      $domaines->nom = $request->input('nom');
      $domaines->description = $request->input('description');
      $domaines->image = "image".$timestamp.".jpg";
      $domaines->logo = "logo".$timestamp.".png";
      $domaines->save();
      return redirect('admin/domaines')->with('status', 'Le domaine a été ajouté avec succès.');
    }

    public function updateDomaines(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'nom' => 'required',
        'description' => 'required',
        'image' => 'mimes:jpeg,jpg,png|image',
        'logo' => 'mimes:jpeg,jpg,png|image',
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $timestamp = Carbon::now()->timestamp;
      $domaines = Domaine::find($request->id);
      if($request->hasFile('image')) {
        unlink('storage/'.$domaines->image.'');
        Image::make($request->file('image'))->resize(1200, 800)->encode('jpg')->save('storage/image'.$timestamp.'.jpg');
        $domaines->image = "image".$timestamp.".jpg";
      }
      if($request->hasFile('logo')) {
        unlink('storage/'.$domaines->logo.'');
        Image::make($request->file('logo'))->resize(512, 512)->encode('png')->save('storage/logo'.$timestamp.'.png');
        $domaines->logo = "logo".$timestamp.".png";
      }
      $domaines->nom = $request->input('nom');
      $domaines->description = $request->input('description');
      $domaines->save();
      return redirect('admin/domaines')->with('status', 'Le domaine a été modifié avec succès.');
    }

    public function deleteDomaines(Request $request)
    {
      unlink('storage/'.$request->logo.'');
      unlink('storage/'.$request->image.'');
      Domaine::destroy($request->id);
      return redirect('admin/domaines')->with('status', 'Le domaine a été supprimé avec succès.');
    }

    // CRUD ADMIN PROMOTION :
    public function getAllPromotions()
    {
      $promotions = Promotion::orderBy('nom', 'asc')->paginate(10);
      $domaines = Domaine::orderBy('nom', 'ASC')->get();
      $domainesPluck = Domaine::orderBy('nom', 'ASC')->pluck('nom', 'id');
      return view('admin.promotions')->with("promotions", $promotions)
                                     ->with("domaines", $domaines)
                                     ->with("domainesPluck", $domainesPluck);
    }

    public function searchPromotions(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'recherche' => 'required',
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $promotions = Promotion::orderBy('nom', 'asc')->where('nom', 'LIKE', '%'.$request->recherche.'%')->orWhere('description', 'LIKE', '%'.$request->recherche.'%')->orWhere('mailresponsable', 'LIKE', '%'.$request->recherche.'%')->orWhereHas('domaine', function($query) use ($request)
      { $query->where('nom', 'LIKE', '%'.$request->recherche.'%'); })->paginate(10);

      $domaines = Domaine::orderBy('nom', 'ASC')->get();
      $domainesPluck = Domaine::orderBy('nom', 'ASC')->pluck('nom', 'id');
      return view('admin.promotions')->with("promotions", $promotions)
                                     ->with("domaines", $domaines)
                                     ->with("domainesPluck", $domainesPluck);
    }

    public function insertPromotions(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'nom' => 'required',
        'description' => 'required',
        'mail' => 'required|email',
        'domaine' => 'required'
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $promotions = new Promotion();
      $promotions->nom = $request->input('nom');
      $promotions->description = $request->input('description');
      $promotions->mailresponsable = $request->input('mail');
      $promotions->domaine_id = $request->input('domaine');
      $promotions->save();
      return redirect('admin/promotions')->with('status', 'La promotion a été ajoutée avec succès.');
    }

    public function updatePromotions(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'nom' => 'required',
        'description' => 'required',
        'mail' => 'required|email',
        'domaine' => 'required'
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $promotions = Promotion::find($request->id);
      $promotions->nom = $request->input('nom');
      $promotions->description = $request->input('description');
      $promotions->mailresponsable = $request->input('mail');
      $promotions->domaine_id = $request->input('domaine');
      $promotions->save();
      return redirect('admin/promotions')->with('status', 'La promotion a été modifiée avec succès.');
    }

    public function deletePromotions(Request $request)
    {
      Promotion::destroy($request->id);
      return redirect('admin/promotions')->with('status', 'La promotion a été supprimée avec succès.');
    }

    // CRUD Admin Administrateur

    public function getAllAdministrateurs()
    {
      $administrateurs = User::orderBy('name', 'asc')->paginate(10);
      return view('admin.administrateurs')->with("administrateurs", $administrateurs);
    }

    public function insertAdministrateurs(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'identité' => 'required',
        'email' => 'required|email|unique:users',
        'motDePasse' => 'required|min:6',
        'confirmationDeMotDePasse' => 'same:motDePasse',
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $administrateurs = new User();
      $administrateurs->name = $request->input('identité');
      $administrateurs->email = $request->input('email');
      $administrateurs->password = Hash::make($request->input('motDePasse'));
      $administrateurs->superAdmin = 0;
      $administrateurs->save();
      return redirect('admin/administrateurs')->with('status', "L'administrateur a été ajoutée avec succès.");
    }

    public function updateAdministrateurs(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'identité' => 'required',
        'email' => 'required|email',
      ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $administrateurs = User::find($request->id);
      $administrateurs->name = $request->input('identité');
      $administrateurs->email = $request->input('email');
      $administrateurs->save();
      return redirect('admin/administrateurs')->with('status', "L'administrateur a été modifié avec succès.");
    }

    public function deleteAdministrateurs(Request $request)
    {
      User::destroy($request->id);
      return redirect('admin/administrateurs')->with('status', "L'administrateur a été supprimé avec succès.");
    }
}
