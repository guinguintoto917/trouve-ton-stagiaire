<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Fiche;
use App\Stage;
use Carbon\Carbon;

class FicheController extends Controller
{
  public function show($id)
  {
    $stage = Stage::findOrFail($id);
    return view('fiche')->with('stage', $stage);
  }

    public function insert(Request $request)
    {
        $validator = Validator::make($request->all(),[
          'nom' => array ('required','regex:/^[a-zA-Z]{3,20}(\s?)[a-zA-Z]{0,20}$/'),
          'prenom' => array('required','regex:/^[a-zA-Z]{3,10}(\s?)[a-zA-Z]{0,10}$/'),
          'stage_id' => 'required',
          'classe' => 'required',
          'lieuDuStage' => 'required',
          'dateDebut' => 'required|date',
          'dateFin' => 'required|date|after:dateDebut',
          'horaires' => 'required',
          'adresse' => 'required',
          'raisonSociale' => 'required',
          'telephone' => array('required' , 'regex:/^0[1-8][0-9]{8}$/'),
          'fax' => array('required','regex:/^[0-9]{3,20}/'),
          'nombreDeSalaries' => array('required','regex:/^[0-9]{1,5}(\s?)[0-9]{0,10}$/'),
          'emailDeEntreprise' => array('required', 'regex:/^([a-zA-Z0-9]{3,20})@([a-zA-Z]{3,20})\.([a-zA-Z]{2,6})$/'),
          'codeAPE' => 'required|min:5|max:5',
          'descriptifDeActivite' => 'required',
          'activiteDurantLeStage' => 'required',
          'adresseDuSiegeSocial'=> 'required',
          'siret' => array('required','regex:/^[0-9]{14}$/'),
          'identiteDuDirigeant' => 'required',
          'identiteDuResponsableDePersonnel' => 'required',
          'nomEtFonctionDuResponsableDeStage' => 'required',
          'captcha' => 'required|captcha',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $stage = Stage::findOrFail($request->input('stage_id'));

        $fiche = new Fiche();
        $fiche->classe = $stage->promotion->nom;
        $fiche->datedebutstage = Carbon::parse($stage->dateDebut)->format('Y-m-d');
        $fiche->datefinstage = Carbon::parse($stage->dateFin)->format('Y-m-d');
        $fiche->etat_id = "1";
        $fiche->stage_id = $request->input('stage_id');
        $fiche->nom = $request->input('nom');
        $fiche->prenom = $request->input('prenom');
        $fiche->lieu = $request->input('lieuDuStage');
        $fiche->horairesprevus = $request->input('horaires');
        $fiche->adresse = $request->input('adresse');
        $fiche->raisonsociale = $request->input('raisonSociale');
        $fiche->telephone = $request->input('telephone');
        $fiche->fax = $request->input('fax');
        $fiche->nbsalaries = $request->input('nombreDeSalaries');
        $fiche->mailentreprise= $request->input('emailDeEntreprise');
        $fiche->codeape= $request->input('codeAPE');
        $fiche->descriptifactivite= $request->input('descriptifDeActivite');
        $fiche->activitestage= $request->input('activiteDurantLeStage');
        $fiche->adressesiege= $request->input('adresseDuSiegeSocial');
        $fiche->numsiret= $request->input('siret');
        $fiche->dirigeants= $request->input('identiteDuDirigeant');
        $fiche->nomresponsablepersonnel= $request->input('identiteDuResponsableDePersonnel');
        $fiche->nomfonctionresponsablestage= $request->input('nomEtFonctionDuResponsableDeStage');
        $fiche->save();


        $pdf = app('Fpdf');
        $pdf->AddPage();  /*créer une page */
        $pdf->Image(asset('storage/logo-pdf.png'),10,10,-300); /*Insérer image */
        $pdf->SetFont('Arial','B',12); /*Police */
        $pdf->Cell(80); /*Décaler cellule */
        $pdf->Cell(400,70,'Fiche d\'identification de stage'); /* afficher cellule(abscisse,ordonnée)*/
        $pdf->Ln(45); /* saut de ligne */
        $pdf->Cell(50);
        $pdf->Cell(30,10,'IDENTIFICATION DE L\'ETUDIANT');
        $pdf->Ln(15); /* saut de ligne*/
        $pdf->Cell(10,10,'Nom :');
        $pdf->Cell(4); /*décaler cellule*/
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('nom'));
        $pdf->Cell(80); /* décalage entre 2 cellules*/
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Prénom :');
        $pdf->Cell(10);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('prenom'));
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Section\Classe :');
        $pdf->Ln(10);
        $pdf->SetFont('');
        $pdf->MultiCell(0,10,$stage->promotion->nom);
        $pdf->Ln(3);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Dates du stage :');
        $pdf->Cell(25);
        $pdf->SetFont('');
        $pdf->Cell(10,10,'Du '.Carbon::parse($stage->dateDebut)->format('d-m-Y').' au '.Carbon::parse($stage->dateFin)->format('d-m-Y'));
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Lieu du stage :');
        $pdf->Cell(22);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('lieuDuStage'));
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Horaires prévus :');
        $pdf->Cell(28);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('horaires'));
        $pdf->Ln(15);
        $pdf->Cell(50);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(30,10,'IDENTIFICATION DE L\'ENTREPRISE');
        $pdf->Ln(15);
        $pdf->Cell(10,10,'Raison sociale et developpement du sigle :');
        $pdf->Ln(10);
        $pdf->SetFont('');
        $pdf->MultiCell(0,10,$request->input('raisonSociale'));
        $pdf->Ln(3);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Adresse complète :');
        $pdf->Cell(31);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('adresse'));
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Téléphone :');
        $pdf->Cell(15);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('telephone'));
        $pdf->Cell(24);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Fax :');
        $pdf->Cell(2);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('fax'));
        $pdf->Cell(24);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Nombre de salariés :');
        $pdf->Cell(33);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('nombreDeSalaries'));
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Mail :');
        $pdf->Cell(3);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('emailDeEntreprise'));
        $pdf->Cell(90);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Code APE :');
        $pdf->Cell(15);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('codeAPE'));
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Activité en clair :');
        $pdf->Cell(25);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('descriptifDeActivite'));
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Numéro de SIRET :');
        $pdf->Cell(31);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('siret'));
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Adresse du siege :');
        $pdf->Cell(30);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('adresseDuSiegeSocial'));
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Dirigeant(s) :');
        $pdf->Cell(18);
        $pdf->SetFont('');
        $pdf->Cell(10,10, $request->input('identiteDuDirigeant'));
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Nom du responsable du personnel :');
        $pdf->Cell(63);
        $pdf->SetFont('');
        $pdf->Cell(10,10,$request->input('identiteDuResponsableDePersonnel'));
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Nom et fonction du responsable de stage :');
        $pdf->Ln(10);
        $pdf->SetFont('');
        $pdf->MultiCell(0,10,$request->input('nomEtFonctionDuResponsableDeStage'));
        $pdf->Ln(30);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(10,10,'Activités proposées durant le stage - sujet du stage :');
        $pdf->Ln(10);
        $pdf->SetFont('');
        $pdf->MultiCell(0,10,$request->input('activiteDurantLeStage'));
        $pdf->Ln(10);
        $pdf->Cell(10,10,'Nom et signature');
        $pdf->Cell(95);
        $pdf->Cell(10,10,'Nom et signature');
        $pdf->Ln(6);
        $pdf->Cell(10,10,'du Maître de stage');
        $pdf->Cell(95);
        $pdf->Cell(10,10,'du Responsable de dispositif *');
        $pdf->Ln(6);
        $pdf->Cell(10,10,'(entreprise)');
        $pdf->Cell(95);
        $pdf->Cell(10,10,'(professeur chargé du suivi)');
        $pdf->Ln(60);
        $pdf->MultiCell(0,7,'*Après accord du Responsable de Dispositif, une convention sera établie en 3 exemplaires');
        Storage::put('pdf'.$fiche->id.'.pdf',$pdf->Output('S')); /* enregistrer le pdf */
        return back()->with('status', 'La fiche a été ajouté avec succès.');

    }

}
