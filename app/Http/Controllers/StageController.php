<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stage;
use App\Promotion;
use DateTime;
use Carbon\Carbon;

class StageController extends Controller
{
    public function index()
    {
      $stage = Stage::all();
      return view('stage.index')->with('stage', $stage);
    }

    public function create()
    {
      // RETURNER LA VUE DE CREATION D'UN STAGE
    }

    public function store(Request $request)
    {
      $stage = new Stage();
      $stage->date = $request->input('date');
      $stage->description = $request->input('description');
      $stage->promotion_id = $request->input('promotion');
      $stage->save();
      // RETOURNER UNE VUE QUI DIT QUE CA A BIEN ETE INSERER
    }

    public function show($id)
    {
      $stage = Stage::all()->where("promotion_id", "=", $id);
      return view('stage.index')->with('stage', $stage);
    }

    public function edit($id)
    {
      // RETOURNE LA VUE D'EDITION D'UN STAGE
    }

    public function update(Request $request, $id)
    {
      $stage = Stage::findOrFail($id);
      $stage->date = $request->input('date');
      $stage->description = $request->input('description');
      $stage->promotion_id = $request->input('promotion');
      $stage->save();
      // RETOURNER UNE VUE QUI DIT QUE CA A BIEN ETE MIS A JOUR
    }

    public function destroy($id)
    {
      Stage::destroy($id);
      // RETOURNER UNE VUE QUI DIT QUE CA A BIEN ETE SUPPRIMER
    }

    public function search(Request $request)
    {
      if (!empty($request->rechercheDate) && empty($request->recherche)) {
        $recherche = Carbon::parse($request->rechercheDate)->format('d-m-Y');
        $stages = Stage::orderBy('id', 'desc')->where('dateDebut', '<=', Carbon::parse($request->rechercheDate)->format('Y-m-d'))->where('dateFin', '>=', Carbon::parse($request->rechercheDate)->format('Y-m-d'))->get();
      } else if (!empty($request->recherche) && !empty($request->rechercheDate)) {
        $recherche = "".Carbon::parse($request->rechercheDate)->format('d-m-Y')." - ".$request->recherche;

        /*
        $stages = Stage::orderBy('id', 'desc')->where(function($query) use ($request->recherche){
            $query->where('description', 'LIKE', '%'.$request->recherche.'%');
        })->orWhere(function($query) use ($request->rechercheDate){
            $query->where('dateDebut', '<=', $request->rechercheDate)->where('dateFin', '>=', $request->rechercheDate);
        })->orWhereHas('promotion', function($query) use ($request)
        { $query->where('nom', 'LIKE', '%'.$request->recherche.'%')->orWhere('description', 'LIKE', '%'.$request->recherche.'%'); })->get();
        */

      } else {
        $recherche = $request->recherche;
        $stages = Stage::orderBy('id', 'desc')->where('description','LIKE','%'.$request->recherche.'%')->orWhereHas('promotion', function($query) use ($request)
        { $query->where('nom', 'LIKE', '%'.$request->recherche.'%')->orWhere('description', 'LIKE', '%'.$request->recherche.'%'); })->get();
      }

      if(!empty($request->statut)) {
        return view('stage.index')->with('stages', $stages)->with('recherche', $recherche)->with('statut', 1);
      } else {
        return view('stage.index')->with('stages', $stages)->with('recherche', $recherche);
      }
    }
}
