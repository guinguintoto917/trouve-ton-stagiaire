<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Domaine;
use App\Stage;
use Carbon\Carbon;

class DomaineController extends Controller
{
     public function accueil()
     {
         $domaines = Domaine::all();
         $stages = Stage::where('dateDebut','>=', Carbon::now()->format('Y-m-d'))->where('dateFin','>=', Carbon::now()->format('Y-m-d'))->orderBy('dateDebut', 'ASC')->take(5)->get();
         return view('accueil')->with('domaines', $domaines)->with('stages', $stages);
     }

    public function index()
    {
        return redirect()->route('accueil');
    }

    public function create()
    {
        return view('domaine.create');
    }

    public function store(Request $request)
    {
        $domaines = new Domaine();
        $domaines->nom = $request->input('nom');
        $domaines->description = $request->input('description');
        $domaines->image = $request->input('image');
        $domaines->save();
    }

    public function show($id)
    {
        $domaines = Domaine::findOrFail($id);
        return view('domaine.show')->with('domaines', $domaines);
    }

    public function edit($id)
    {
        $domaine = Domaine::findOrFail($id);
        return view('domaine.edit')->with($domaine);
    }

    public function update(Request $request, $id)
    {
        $domaines = Domaine::findOrFail($id);
        $domaines->nom = $request->input('nom');
        $domaines->description = $request->input('description');
        $domaines->image = $request->input('image');
        $domaines->save();
    }

    public function destroy($id)
    {
        Domaine::destroy($id);
    }
}
