<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contact;
use App\Mail\ContactPP;
use App\Promotion;
use App\Mail\Test;
use App\Mail\Renseignement;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function sendEmailtoUser()
    {
       $to_email = "guinguintoto917@gmail.com";

       Mail::to($to_email)->send(new Contact);

       return "<p>Votre email a été envoyé avec succès.</p>";
    }

    public function demandeRenseignement(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'email' => array('required', 'regex:/^([a-zA-Z0-9.]{3,30})@([a-zA-Z]{3,25})\.([a-zA-Z]{2,6})$/'),
        'message' => 'required|max:1500',
        'identite' => array('required','regex:/^[a-zA-Z]{2,20}(\s?)[a-zA-Z]{0,20}$/'),
        'captcha' => 'required|captcha',
      ]);

      if ($validator->fails()) {
        return redirect('/#contact')->withErrors($validator)->withInput();
      }

       $to_email = "trouvetonstagiairetts@gmail.com";

       Mail::to($to_email)->send(new Renseignement($request));

       return redirect('/#contact')->with('status', 'Votre message a bien été envoyé. Votre demande sera traitée dans les meilleurs délais.');
    }

    public function contactPP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idPromotion' => 'required',
            'emailPP' => 'required',
            'email' => array('required','regex:/^([a-zA-Z0-9.]{3,30})@([a-zA-Z]{3,25})\.([a-zA-Z]{2,6})$/'),
            'message' => 'required|max:1500',
            'identite' => array('required','regex:/^[a-zA-Z]{2,20}(\s?)[a-zA-Z]{0,20}$/'),
            'captcha' => 'required|captcha',
        ]);

        if ($validator->fails()) {
            return redirect('/contact/'.$request->input('idPromotion').'')->withErrors($validator)->withInput();
        }

        $to_email = $request->input('emailPP');

        Mail::to($to_email)->send(new ContactPP($request));

        return redirect('/contact/'.$request->input('idPromotion').'')->with('status', 'Votre message a bien été envoyé. Votre demande sera traitée dans les meilleurs délais.');
    }
}
