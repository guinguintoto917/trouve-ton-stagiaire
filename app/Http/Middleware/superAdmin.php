<?php

namespace App\Http\Middleware;

use Closure;

class superAdmin
{
    public function handle($request, Closure $next)
    {
      if ($request->user()->superAdmin != 1) {
          return redirect('admin');
      }

      return $next($request);
    }
}
