<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domaine extends Model
{
    public function promotion()
    {
      return $this->hasMany('App\Promotion');
    }
}
