<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fiche extends Model
{
    public function fiche ()
    {
      return $this->belongsTo('App\Stage');
    }

    public function etat()
    {
        return $this->belongsTo('App\Etat');
    }
}
