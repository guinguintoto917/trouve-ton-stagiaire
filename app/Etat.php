<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etat extends Model
{
    public function fiche()
    {
        return $this->hasMany('App\Fiche');
    }
}
