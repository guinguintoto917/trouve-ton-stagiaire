<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class Renseignement extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function build()
    {
        return $this->from('trouvetonstagiairetts@gmail.com')
                    ->view('mail.renseignement')
                    ->with([
                        'email' => $this->request->email,
                        'identite' => $this->request->identite,
                        'bodyMessage' => $this->request->message,
                    ]);
    }
}
