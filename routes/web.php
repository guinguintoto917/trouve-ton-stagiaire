<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('/editor', 'CKEditorController');

Route::get('/', "DomaineController@accueil")->name('accueil');
Route::post('/search', "StageController@search")->name('search');
Route::resource('domaine', 'DomaineController');
Route::resource('stage', "StageController");
Route::resource('promotion',"PromotionController");
Route::get('/mail',"ContactController@sendEmailtoUser");
Route::get('/contact/{id}',"PromotionController@contactPP")->name('contactPromotion');
Route::post('/contactPP',"ContactController@contactPP")->name('contactPP');
Route::post('/renseignement',"ContactController@demandeRenseignement")->name('renseignement');
Route::get('/fiche/{id}',"FicheController@show")->name('showFiche');
Route::post('/fiche',"FicheController@insert")->name('insertFiche');


// Blocage
Route::get('/fiche', function () { return redirect('/'); });
Route::get('/search', function () { return redirect('/'); });

// Route d'authentification
Auth::routes(['register' => false], ['reset' => false]);
// Route d'administration
Route::GET('/admin', 'AdminController@index')->name('admin');
// CRUD Stage Admin
Route::GET('/admin/stages', 'AdminController@getAllStages')->name('stages');
Route::POST('/admin/stages', 'AdminController@searchStages')->name('searchStages');
Route::POST('/admin/stages/insert', 'AdminController@insertStages')->name('insertStages');
Route::DELETE('/admin/stages/delete', 'AdminController@deleteStages')->name('deleteStages');
Route::PUT('/admin/stages/update', 'AdminController@updateStages')->name('updateStages');
// CRUD Domaine ADMIN
Route::GET('/admin/domaines', 'AdminController@getAllDomaines')->name('domaines');
Route::POST('/admin/domaines', 'AdminController@searchDomaines')->name('searchDomaines');
Route::POST('/admin/domaines/insert', 'AdminController@insertDomaines')->name('insertDomaines');
Route::DELETE('/admin/domaines/delete', 'AdminController@deleteDomaines')->name('deleteDomaines');
Route::PUT('/admin/domaines/update', 'AdminController@updateDomaines')->name('updateDomaines');
// CRUD Promotion ADMIN
Route::GET('/admin/promotions', 'AdminController@getAllPromotions')->name('promotions');
Route::POST('/admin/promotions', 'AdminController@searchPromotions')->name('searchPromotions');
Route::POST('/admin/promotions/insert', 'AdminController@insertPromotions')->name('insertPromotions');
Route::DELETE('/admin/promotions/delete', 'AdminController@deletePromotions')->name('deletePromotions');
Route::PUT('/admin/promotions/update', 'AdminController@updatePromotions')->name('updatePromotions');
// CRUD Administrateurs ADMIN
Route::GET('/admin/administrateurs', 'AdminController@getAllAdministrateurs')->name('administrateurs')->middleware('superAdmin');
Route::POST('/admin/administrateurs/insert', 'AdminController@insertAdministrateurs')->name('insertAdministrateurs')->middleware('superAdmin');
Route::DELETE('/admin/administrateurs/delete', 'AdminController@deleteAdministrateurs')->name('deleteAdministrateurs')->middleware('superAdmin');
Route::PUT('/admin/administrateurs/update', 'AdminController@updateAdministrateurs')->name('updateAdministrateurs')->middleware('superAdmin');
// CRUD Fiche ADMIN
Route::GET('/admin/fiches', 'AdminController@getAllFiches')->name('fiches');
Route::POST('/admin/fiches', 'AdminController@searchFiches')->name('searchFiches');
Route::PUT('/admin/fiches/update', 'AdminController@updateFiches')->name('updateFiches');
